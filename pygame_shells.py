import pygame as pg
from perlin_noise import PerlinNoise
import time
import math

def project_point(point: pg.Vector3):
    x = point.x * (ncp / point.z)
    y = point.y * (ncp / point.z)

    return pg.Vector2(x, y)

def cartesian(point: pg.Vector2):
    x = point.x + half_bounds[0]
    y = -point.y + half_bounds[1]

    return pg.Vector2(x, y)

def draw_rect(pos: pg.Vector3, color):
    projected_points = []

    for rect_point in rect_points:
        offset = rect_point * width
        point = pos + offset
        projected = project_point(point)
        projected_points.append(cartesian(projected * zoom))

    pg.draw.polygon(screen, color, projected_points)


def lerp_colors(t: float):
    new_color = []
    for i in range(3):
        lerp = color1[i] + t * (color2[i] - color1[i])
        new_color.append(lerp)

    return tuple(new_color)

def draw_layer(i, layer_percent, offset: pg.Vector3):
    rect_count = 0

    for x in range(grid_size[0]):
        for y in range(grid_size[1]):
            if noise_values[x][y] < layer_percent:
                continue
            
            draw_rect(start_pos + pg.Vector3(x * width, i * layer_height, y * width) + offset, colors[i])

            rect_count += 1

rect_points = (pg.Vector3(0, 0, 0), pg.Vector3(1, 0, 0), pg.Vector3(1, 0, 1), pg.Vector3(0, 0, 1))

color1 = (57, 79, 23)
color2 = (173, 227, 25)

window_res = 110
bounds = (16 * window_res, 9 * window_res)
half_bounds = (bounds[0] / 2, bounds[1] / 2)

ncp = 20
zoom = 50
width = 2
grid_size = (200, 200)
start_pos = pg.Vector3(-grid_size[0] / 2 * width, -250, 500)
scale = 40

layer_count = 16
layer_height = 2

noise = PerlinNoise(octaves=10, seed=1)
noise_offset = 0.4

wave_scale = 5
wave_speed = 5

t = 0
t_step = 0.01

pg.init()
screen = pg.display.set_mode(bounds)
clock = pg.time.Clock()

noise_values = []
for x in range(grid_size[0]):
    noise_values.append([])
    for y in range(grid_size[1]):
        noise_values[x].append(noise([x / scale + t / 10, y / scale + t / 10]) + noise_offset)

colors = []
for i in range(layer_count):
    color = lerp_colors(i / layer_count)
    rect_color = pg.Color(int(color[0]), int(color[1]), int(color[2]))
    colors.append(rect_color)
    
print("noise generated")

while True:
    screen.fill("salmon")

    start_time = time.time()
    wave = math.sin(t * wave_speed) * wave_scale

    for i in range(layer_count):
        layer_percent = (1 / layer_count) * i
        
        draw_layer(i, layer_percent, pg.Vector3(wave * layer_percent, 0, 0))

    pg.display.flip()
    clock.tick(60)

    t += t_step

    total_time = time.time() - start_time
    print("Total Draw Time: " + str(total_time))
